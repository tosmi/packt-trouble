# create ssh directories
class trouble::ssh {
  file { '/home/trouble':
    ensure => 'directory',
    mode => '0755',
    owner => 'trouble',
    group => 'trouble',
  }

  file { '/home/trouble/.ssh':
    ensure => 'directory',
    mode => '0700',
    owner => 'trouble',
    group => 'trouble',
  }

  file { '/home/trouble/.ssh/authorized_keys':
    ensure => 'file',
    mode => '0600',
    owner => 'trouble',
    group => 'trouble',
  }
} 
